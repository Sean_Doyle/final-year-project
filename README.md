# **Moodle Tourguide Designer** #

## The Project: ##
This project is a Moodle Plugin that allow administrative teams to produce tourguides that will overlay pages within Moodle to help better instruct and inform the end user on how to use the various functionality that Moodle provides.

## What is Moodle? ##
Moodle is a learning platform designed to provide educators, administrators and learners with a single robust, secure and integrated system to create personalized learning environments.

## Components ##
### assets/js: ###
This is the where the scripts that provide all the client side functionality reside. 

### db: ###
Contains the xml file that Moodle will use to create the plugin table in its database.



## Version: 2015041719 ##

# Current Setup Steps #

* Download and rename the project folder to "*tourguide*"
* Put the folder into the Moodle/local folder
* Log into Moodle as an Admin and check for updates, Moodle should prompt for install
* Allow the installation to add current table into Moodle DB
* On any page go to -> Site Administration -> Appearance -> Additional HTML
* Include the following HTML code in the footer: *<script type="text/javascript" src="../local/tourguide/assets/js/setup.js"></script>*