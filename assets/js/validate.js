
var url = "";
var guide;
var error = [];

/**
 * Creates a fresh guide object with an ID
 */ 
function initGuide(){
	guide = {};
	guide.id = new Date().getTime();
	guide.title = "";
	guide.steps = [];

	errorDiv.innerHTML = error;
}

/**
 * Checks if the inital info for the guide has any errors -> reports errors if found, else add the details to the guide object
 */
function storeHead(){
	initGuide();

	var tempURL = document.getElementById("URL-entry").value;
	if(tempURL === ""){ tempURL = document.URL; }
	url = parseURL(tempURL);
	if(url === "") error.push("Invalid Location it is not registered in the system.");

	if(validate("title",document.getElementById("title-entry").value)){
		guide.title = document.getElementById("title-entry").value;
	}

	if(error === ""){ errorDiv.innerHTML = ""; addStep(); } 
	else { reportError(); error = []; }
}	

/**
 * Checks if the current step has any errors -> report errors if found, else add step to steps array and return true;
 */
function storeStep(){
	var markdownConverter = new Showdown.converter();
	var step = {};
	if(validate("target",document.getElementById("target-entry").value)){
		step.target = document.getElementById("target-entry").value;
	}
	if(validate("description",document.getElementById("info-entry").value)){
		step.content = markdownConverter.makeHtml(document.getElementById("info-entry").value);
	}
	step.direction = document.querySelector('input[name="direction"]:checked').value.toLowerCase();
	step.shadow = document.querySelector('input[name="shadow"]:checked').value.toLowerCase();

	if(error === ""){ guide.steps.push(step); errorDiv.innerHTML = ""; addStep();	} 
	else { reportError(); error = []; return false; }
	
	return true;
}

/**
 * Checks if field is empty and if so add an error message to the error array
 */
function validate(field,data){ 
	if(data !== ""){
		return true;
	} else {
		error.push("Invalid entry for : " + field);
	}
}

/**
 * Ajax call to server to store guide
 */
function saveGuide(){
	if(storeStep()){ //No errors in current step;
        $.ajax({
        	type:'POST',
        	url: '../local/tourguide/serverRequests.php',
        	data: { 'url':url, 'guide':JSON.stringify(guide) },
        	success: function(){
				initGuide();
				location.href="#close";
        	}	
        });
    }
}

/**
 * Checks if url is a match in the urlKeys
 */
function parseURL(urlToCheck){
	var tempURL = "";
	$.each( urlKeys, function(index){
		if(urlToCheck.indexOf(urlKeys[index]) != -1){
			tempURL = urlKeys[index];
			return false;
		}
	});
	return tempURL;
}

/**
 * Constructs a list of all the current errors on the page;
 */
function reportError(){
	var errList = "";
	error.forEach(function(entry){
		errList += entry + "\n";
	});

	alert(errList);
}

