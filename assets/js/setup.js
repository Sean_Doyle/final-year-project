//Anchor Point for guide button
var anchor = document.getElementById("expandable_branch_71_siteadministration");

//Head tag
var head = document.getElementsByTagName("head")[0];

//Footer tag
var footer = document.getElementsByTagName("footer")[0];

//Attach scripts
head.appendChild(includeCSS("modal.css")); //Modal CSS
head.appendChild(includeCSS("pageguide.css")); //Pageguide CSS

footer.appendChild(includeJS("jquery.pageguide.js")); //Pageguide functionality
footer.appendChild(includeJS("showdown.js"));
footer.appendChild(includeJS("selectorTool.js")); //Tool for selecting IDs from page
footer.appendChild(includeJS("urlKeys.js")); //File for the URL segments to do matching
footer.appendChild(includeJS("designer.js")); //Modal JS + setup
footer.appendChild(includeJS("validate.js")); //Input validation
footer.appendChild(includeJS("guideCheck.js")); //URL Testing + Guide Retrieval


//Button for modal activation
var btn = document.createElement("button");
btn.setAttribute("id","runTourguide");
btn.setAttribute("onClick",'openDesigner()');
btn.innerHTML = "TourguideDesigner";
anchor.parentNode.insertBefore(btn,anchor);

function includeCSS(fileName){
	var stylesheet = document.createElement("link");
	stylesheet.setAttribute("rel","stylesheet");
	stylesheet.setAttribute("type","text/css");
	stylesheet.setAttribute("href","../local/tourguide/assets/css/" + fileName);

	return stylesheet;
}

function includeJS(fileName){
	var script = document.createElement("script");
	script.setAttribute("type","text/javascript");
	script.setAttribute("src","../local/tourguide/assets/js/" + fileName);

	return script;
}

