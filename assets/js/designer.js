//Main Div
var modalBox;

//Inner Wrapper
var innerDiv;

//Exit Button
var exit;

//Title on the designer
var headerDiv;

//Error Report
var errorDiv;

//Run setup as soon as script loads
setup();

/**
 * Creates the base modal html
 */
function setup(){
	modalBox = document.createElement("div");
	modalBox.setAttribute("id","openModal");
	modalBox.setAttribute("class","modalDialog");

	innerDiv = document.createElement("div");
	innerDiv.setAttribute("id","modal-innerWrapper");

	exit = document.createElement("a");
	exit.setAttribute("href","#close");
	exit.setAttribute("title","Close");
	exit.setAttribute("class","close");
	exit.innerHTML = "X";

	errorDiv = document.createElement("div");
	errorDiv.setAttribute("id","modal-error");

	headerDiv = document.createElement("div");
	headerDiv.setAttribute("id","modal-headerDiv");

	modalBox.appendChild(innerDiv);
	anchor.parentNode.insertBefore(modalBox,anchor);
}

/**
 * Creates the homepage html on the modal
 */
function openDesigner(){
	//Clear the Box
	innerDiv.innerHTML = "";

	headerDiv.innerHTML = "<h2>Guide Designer</h2><hr><br />";

	var createBtn = document.createElement("button");
	createBtn.setAttribute("onclick","guideDesigner()");
	createBtn.innerHTML = "Create a Guide";

	var availBtn = document.createElement("button");
	availBtn.setAttribute("onclick","guideInfo()");
	availBtn.innerHTML = "View Guide Info";

	var deleteBtn = document.createElement("button");
	deleteBtn.setAttribute("onclick","editKeys()");
	deleteBtn.innerHTML = "Options";

	innerDiv.appendChild(exit);
	innerDiv.appendChild(headerDiv);
	innerDiv.appendChild(errorDiv);
	innerDiv.appendChild(createBtn);
	innerDiv.appendChild(availBtn);
	innerDiv.appendChild(deleteBtn);
	innerDiv.innerHTML += "<br /><br /><hr><br />";

	//Open Modal
	location.href="#openModal";
}

/**
 * Creates the html for initial guide creation page
 */
var stepNum;
function guideDesigner(){
	stepNum = 0;
	
	//Clear the Box
	innerDiv.innerHTML = "";

	headerDiv.innerHTML = "<h2>Guide Designer</h2><hr><br />";

	//URL
	var urlDiv = document.createElement("div");
	urlDiv.setAttribute("id","modal-urlDiv");
	urlDiv.innerHTML = 'URL: <input id="URL-entry" type="text" placeholder="Destination: (Leave blank if for this page)" size="35">';

	//Title
	var titleDiv = document.createElement("div");
	titleDiv.setAttribute("id","modal-titleDiv");
	titleDiv.innerHTML = 'Title: <input id="title-entry" type="text" placeholder="Title of the guide:" size="35">';

	//BeginBtn
	var beginBtnDiv = document.createElement("div");
	beginBtnDiv.setAttribute("id","modal-beginBtnDiv");
	beginBtnDiv.innerHTML = '<button id="btn-begin" onclick="storeHead()">Begin</button><button onclick="openDesigner()">Return</button>';

	//Attach elements
	innerDiv.appendChild(exit);
	innerDiv.appendChild(headerDiv);
	innerDiv.appendChild(errorDiv);
	innerDiv.appendChild(urlDiv);
	innerDiv.appendChild(titleDiv);
	innerDiv.innerHTML += "<br /><hr><br />";
	innerDiv.appendChild(beginBtnDiv);
}

/**
 * Ajax call to get all guides in database
 */
function guideInfo(){
	$.ajax({
		type:'GET',
		url: '../local/tourguide/serverRequests.php',
		data: { 'getGuides': "guides" },
		success: function(data){
			listGuides(data);
		}
	});
}

/**
 * Creates html for the Options section
 */ 
function editKeys(){
	//Clear the Box
	innerDiv.innerHTML = "";

	headerDiv.innerHTML = "<h2>Edit Page Keys</h2><hr><br />";

	var p = document.createElement("p");
	p.innerHTML = "Here you can add, remove or edit the keys that signify Moodle URLs that allow guides to be created on. There is no system undo/rollback, so play with care!";

	var keyField = document.createElement("textarea");
	keyField.setAttribute("id","keys");
	keyField.setAttribute("rows","12");
	keyField.setAttribute("cols","85");

  for(var index in urlKeys){
    if(urlKeys.hasOwnProperty(index)){
    	keyField.innerHTML += urlKeys[index] + "\n";
    }
  }

  
	//Save button
	var svbtn = document.createElement("button");
	svbtn.setAttribute("onclick","saveKeys()");
	svbtn.innerHTML = "Save";

	//Return button
	var retbtn = document.createElement("button");
	retbtn.setAttribute("onclick","openDesigner()");
	retbtn.innerHTML = "Return";

	//Attach elements
	innerDiv.appendChild(exit);
	innerDiv.appendChild(headerDiv);
	innerDiv.appendChild(p);
	innerDiv.appendChild(keyField);
	innerDiv.innerHTML += "<br /><hr><br />";	
	innerDiv.appendChild(svbtn);
	innerDiv.appendChild(retbtn);
}

/**
 * Creates the HTML for the Step information page
 */
function addStep(){
	stepNum ++;

	//Clear the Box
	innerDiv.innerHTML = "";

	headerDiv.innerHTML = "<h2>Step " + stepNum + "</h2><hr><br />";

	//Target
	var targetDiv = document.createElement("div");
	targetDiv.setAttribute("id","modal-targetDiv");
	targetDiv.innerHTML = 'Target: <input id="target-entry" type="text" placeholder="Target of the step (CSS Selectors)" size="35"><button id="elementSelector" onclick="startSelector()">SelectorTool</button>';

	//Info
	var descriptionDiv = document.createElement("div");
	descriptionDiv.setAttribute("id","modal-descriptionDiv");
	descriptionDiv.innerHTML = 'Description:<br>  <textarea id="info-entry" rows="12" cols="85" placeholder="Information to give the user:"></textarea>';

	//Direction
	var directionDiv = document.createElement("div");
	directionDiv.setAttribute("id","modal-directionDiv");
	directionDiv.innerHTML = 'Direction:';
	directionDiv.innerHTML += '<input type="radio" name="direction" class="visuallyhidden" value="Top" checked>Top';
	directionDiv.innerHTML += '<input type="radio" name="direction" class="visuallyhidden" value="Bottom">Bottom';
	directionDiv.innerHTML += '<input type="radio" name="direction" class="visuallyhidden" value="Left">Left';
	directionDiv.innerHTML += '<input type="radio" name="direction" class="visuallyhidden" value="Right">Right';

	//Shadow
	var shadowDiv = document.createElement("div");
	shadowDiv.setAttribute("id","modal-shadowDiv");
	shadowDiv.innerHTML = 'Shadow:';
	shadowDiv.innerHTML += '<input type="radio" name="shadow" class="visuallyhidden" value="True" checked>True';
	shadowDiv.innerHTML += '<input type="radio" name="shadow" class="visuallyhidden" value="False">False';

	//Btns
	var btnDiv = document.createElement("div");
	btnDiv.setAttribute("id","modal-btnDiv");
	btnDiv.innerHTML = '<button id="btn-begin" onclick="storeStep()">Add another step</button><button id="btn-begin" onclick="saveGuide()">Finished</button>';

	//Attach elements
	innerDiv.appendChild(exit);
	innerDiv.appendChild(headerDiv);
	innerDiv.appendChild(errorDiv);
	innerDiv.appendChild(targetDiv);
	innerDiv.appendChild(descriptionDiv);
	innerDiv.appendChild(directionDiv);
	innerDiv.appendChild(shadowDiv);
	innerDiv.innerHTML += "<br /><hr><br />";
	innerDiv.appendChild(btnDiv);
}

/**
 * Creates the HTML for showing all the guides currently in the database in table form
 */
function listGuides(guides){
	//Clear the Box
	innerDiv.innerHTML = "";

	headerDiv.innerHTML = "<h2>Guide Information</h2><hr><br />";

	guides = JSON.parse(guides);

	var table = document.createElement("table");
	table.setAttribute("style","width: 100%;");
	table.innerHTML = "<tr><th> Guide Title </th><th> URL </th><th> Guide ID </th></tr>";

	for(var index in guides){
		if(guides.hasOwnProperty(index)){
			var guide = JSON.parse(guides[index].json);

			var tableRow = document.createElement("tr");
			tableRow.innerHTML = "<td>" + guide.title + "</td><td>" + guides[index].url + "</td><td>" + guides[index].id + "</td><td><a id='" + guides[index].id + "' href='" + document.URL + "' onclick=deleteGuide(this.id)>DELETE</a></td>";
			table.appendChild(tableRow);
		}
	}

	//Return button
	var btn = document.createElement("button");
	btn.setAttribute("onclick","openDesigner()");
	btn.innerHTML = "Return";

	//Attach elements
	innerDiv.appendChild(exit);
	innerDiv.appendChild(headerDiv);
	innerDiv.appendChild(table);
	innerDiv.innerHTML += "<br /><hr><br />";
	innerDiv.appendChild(btn);
}

/**
 * Ajax call to delete a guide from the database
 */ 
function deleteGuide(id){
	$.ajax({
		type:'POST',
		url: '../local/tourguide/serverRequests.php',
		data: { 'delete': id },
		success: function(data){
			openDesigner();
		}
	});
}

/**
 * Formats the text area value into a set of strings to be sent to the server via Ajax POST to be stored in the javascript file.
 */ 
function saveKeys(){
	var currentKeys = document.getElementById("keys").value;
	urlKeys = currentKeys.trim().split("\n"); //Set the local url keys to the new list
	currentKeys = urlKeys;
	var keyString = "";

	for(var i in currentKeys){
		if(currentKeys.hasOwnProperty(i)){
			currentKeys[i] = "\"" + currentKeys[i] + "\",";
			keyString += currentKeys[i];
		}
	}

	$.ajax({
		type:'POST',
		url: '../local/tourguide/serverRequests.php',
		data: { 'keys': keyString },
		success: function(data){
			openDesigner();
		}
	});
}