var pageURL = document.URL;

function checkIfGuideExists(){
	var urlToCheck = parseURL(pageURL);
	if(urlToCheck !== ""){
		$.ajax({
			type:'GET',
			url: '../local/tourguide/serverRequests.php',
			data: { 'guideCheck': urlToCheck },
			success: function(data){
				data = JSON.parse(data);
				if(data.found){ reconstructGuide(JSON.parse(data.guide)); }
				else { console.log("No guide found for current page"); }
			}
		});
	}
}

/**
 * Guide is stored as text on database, function turns string back into json object and loads the guide
 */
function reconstructGuide(tour){
	var guide = {};
	guide.id = tour.id;
	guide.title = tour.title;
	guide.steps = [];

	for(var index in tour.steps){
		if(tour.steps.hasOwnProperty(index)){
			var step = {};
			step.target = tour.steps[index].target;
			step.content = tour.steps[index].content;
			step.direction = tour.steps[index].direction;
			step.shadow = JSON.parse(tour.steps[index].shadow);

			guide.steps.push(step);
		}
	}

	$.pageguide(guide);
}

checkIfGuideExists();