function startSelector(){
	location.href="#close"; //Close Modal

	setTimeout(function() {
		$(window).on("click.selector", function(e) {
			e.preventDefault();
			var x = e.clientX, y = e.clientY;
		    
		    var trgt = document.elementFromPoint(x, y).id;
			if(trgt.match("yui")){
		    	console.log("invalid ID - Target Cannot Be YUI");
		    } else {
		    	$(window).off("click.selector");
		    	document.getElementById("target-entry").value = "#" + trgt;
		    	location.href="#openModal"; //Reopen Modal
		    }	    
		});
	}, 500);//Timeout to allow modal box to close.
}
