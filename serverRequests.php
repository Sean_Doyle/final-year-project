<?php

	$servername = "localhost";
	$username = "moodle_edge";
	$password = "admin";
	$dbname = "moodle_edge";

	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error){
		die("Connection failed: " . $conn->connect_error);
	}

	if(isset($_POST['guide'])){ //Create Guide POST
		pushGuideToDB();
	}

	if(isset($_GET['guideCheck'])){ //Check for Guide GET
		checkForGuide();
	}

	if(isset($_GET['getGuides'])){ //Retrieve Guides GET
		returnGuides();
	}

	if(isset($_POST['delete'])){ //Delete Guide POST
		deleteGuide();
	}

	if(isset($_POST['keys'])){ //Edit current url key file
		editKeys();
	}

	/**
	 * Using a prepared stmt pushes the guide into the database
	 */
	function pushGuideToDB(){
		global $conn;

		$id = time();
		$url = $_POST['url'];
		$json = $_POST['guide'];

		$stmt = $conn->prepare("INSERT INTO mdl_local_tourguide(id,url,json) VALUES(?,?,?)");
		$stmt->bind_param("iss",$id,$url,$json);
		$stmt->execute();
	}

	/**
	 * Using a prepared stmt checks the database for a guide on the sent url
	 */
	function checkForGuide(){
		global $conn;

		$urlToCheck = $_GET['guideCheck'];
		$stmt = $conn->prepare("SELECT json FROM mdl_local_tourguide WHERE url=?");
		$stmt->bind_param("s",$urlToCheck);
		$stmt->execute();

		$result = $stmt->get_result();
		$result = $result->fetch_array(MYSQLI_NUM);

		if(count($result) != 0) {
			$json = array("found" => "true", "guide" => $result[0]);
		} else {
			$json = array("found" => "false");
		}
		echo (json_encode($json));
	}

	/**
	 * Using a prepared stmt obtains all the guides currently in the database and returns them to the client
	 */
	function returnGuides(){
		global $conn;

		$stmt = $conn->prepare("SELECT * FROM mdl_local_tourguide");
		$stmt->execute();

		$result = $stmt->get_result();
		$json = mysqli_fetch_all ($result, MYSQLI_ASSOC);
		echo json_encode($json );
	}	

	/**
	 * Using a prepared stmt deletes a guide with the ID that was sent by the client
	 */
	function deleteGuide(){
		global $conn;

		$id = $_POST['delete'];
		$stmt = $conn->prepare("DELETE FROM mdl_local_tourguide WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
	}

	/**
	 * Edit the urlKeys.js file server side to the new values entered client side
	 */
	function editKeys(){
		$file = "assets/js/urlKeys.js";

		$keys = $_POST['keys'];
		$fhandle = fopen($file,"w");
		$contents = "var urlKeys = [" . $keys . "]";

		echo($contents);
		fwrite($fhandle,$contents);
		fclose($fhandle);
	}
?>